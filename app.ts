 (function(){

// const nombre = 'Fausto';
// const apellido = 'Maluf';
// const edad = '29';
// //  Ejemplos De Template Literals. 
// // \n: es un salto de linea
// const salida = `${ nombre } \n ${ apellido } \n (${ edad })`;

// console.log(salida);

// // Funciones: parametros opcionales, oblogatorios y por defecto.

// function activar(quien: string, objeto: string = 'batiseñal', momento?: string ){
//   console.log(`${ quien } activo la ${ objeto } en la ${ momento }`);
  
// }

// activar('Gordon');

// // funciones de flechas

// let miFuncion = function(a: string){
//   // funcion tradicionl
//   return a.toUpperCase();
// }

// const miFuncionF = (a:string ) => a.toUpperCase(); 
// const sumarN = function(a:number, b:number) {
//   return a + b;
// }
// const sumarF = (a:number, b:number) => a + b;
  // funcion de flecha
// console.log(miFuncion('Normal'));
// console.log(miFuncionF('Flecha'));
// console.log(sumarF(9 , 6));

// const hulk = {
//   nombre: 'Hulk',
//   smash(){
//     console.log(`${this.nombre} smash!!!`);
    
//   }
// }

// hulk.smash();

// Desestructuracion de objetos y arreglos

// const Avenger = {
// nombre: 'steve',
// clave: 'capitan america',
// poder: 'escudo'
// }

// const extraer = ({ nombre, poder }: any ) => {
//   // const {  nombre , poder } = Avenger;


//   console.log(Avenger.nombre);
//   console.log(Avenger.poder);
// }

// extraer(Avenger);
// const Avenger: string[]=['thor','ironman','spiderman'];


// const [loki, stark, peter] = Avenger;

// console.log(loki);
// console.log(stark);
// console.log(peter);
      //  EJEMPLO1 DE UNA PROMESA////
// console.log('Inicio');
// const prom1 = new Promise((resolve, reject)=>{
//    setTimeout(() => {
//      resolve('Se termino el tiempo')
//    }, 1000)
// });

// prom1
//      .then(mensaje => console.log( mensaje ))
//      .catch(err => console.warn(err));
     

// console.log('Fin');


        ///// EJAMPLE2 PROMESAS

      //   const retirarDinero = (montoRetirar: number): Promise<number> => {
      //     let dineroActual = 1000;
         
          
      //     return new Promise((resolve,reject) => {
      //      if (montoRetirar > dineroActual){
      //        reject('no hay dinero suficiente')
      //      } else {
      //        dineroActual -= montoRetirar;
      //        resolve( dineroActual);
      //      }
      //     });
      //   }

      // retirarDinero( 500 )
      // .then( montoActual => console.log(`me queda ${montoActual}`))
      // .catch( err => console.warn( err))
      
    







})();



