let apples: number = 5;
let speed: string = 'fast';

let nothingMuch: null = null;
let nothing: undefined = undefined;

//  build in objects
 let now: Date = new Date();

//  Array

let colors: string[] = ['red', 'green', 'blue'];
let myNumbers: number[] = [1,2,3];
let truths: boolean[] = [true, true, false];


// classes

class Car {

}

let car: Car = new Car(); //In this case I want to make an instance of class car so I'm going to say that my type is capital c car.
//a variable that's referring to an instance of a car.





//****** */ Object literals *********
// I want to try creating an object and adding in a type notation for it going to first create the object
// So to indicate that we're going to assign an object that has an x property that's a number and a y property

// to number two the variable point we're gonna put the colon and then a set of curly braces and then we'll

// list out all the different property names with their respective types right inside of that set of curly

// braces.


let point: { x: number; y: number } = {
    x: 10,
    y: 20
};


// ***** Function***** 

const logNumber: (i: number) => void = (i: number) => {
   console.log(i)
}

// on the left is a description of a function and then on the right hand side of the equals sign is where we actually.


// When to use  annotations
//  1) Function returns the 'any' 
// in this example JSON.parse returns the any type.

const json = '{"x": 10, "y": 20}';
const coordinates: {x: number; y:number} = JSON.parse(json);
console.log(coordinates); // {x:10, y:20}


// 2) When we declare a variable on oneline 
// and initializate later

let words = ['red', 'green', 'blue'];
let foundWord: boolean;

for(let i = 0; i < words.length; i++) {
    if(words[i] === 'green') {
       foundWord = true;
    }
}

// 3) Variables whose type cannot inferred correctly.
let numbers = [-10, -1, 12];
let numberAboveZero: boolean | number = false;

for (let i = 0; numbers.length; i++) {
    if(numbers[i] > 0){
        numberAboveZero = numbers[i];
    }
}

