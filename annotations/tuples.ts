const drink = {
color: 'brown',
carbonated: true,
sugar: 40
};

// type alias

type Drink = [string, boolean, number];

const pepsi: Drink = ['brown', true, 40]; // array is going to always have a very consistent ordering of elements inside
const sprite: Drink = ['clear', true, 40]