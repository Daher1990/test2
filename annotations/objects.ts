const profile = {
    name: 'alex',
    age: 20,
    coords:{
        lat: 0,
        lng: 15
    },
    setAge(age: number): void{
      this.age = age;
    }

};

const { age }: {age: number} = profile;  //to use destructuring with type annotation you must put the structure of the object in this case profile.
const {coords: {lat, lng}}: {coords:{lat:number; lng:number}} = profile;