// arrow function

const add = (a: number, b: number): number => {
  return a + b;
};

// named function

function divide(a: number, b:number): number{
  return a/b;
}


// anonimus function asigned to a variable.
const multiply = function(a:number, b: number): number {
    return a * b;
}